# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository holds the source code for DharaGithubApp. The following features are incorporated in this application.
- Search for repositories (not yet implemented)
- Get a list of all public repositories
- View the details of the repository

* Version - 1.0.0

### How do I get set up? ###

* Configuration
- Gradle is used to build the application.
- Java 1.8 is required for Lambda expressions
- Latest android studio

* Dependencies
The following libraries have been used:
- Appcompat library
- RecyclerView library
- GSON
- Glide for loading the images
- Retrofit to make API calls
- OkHttp used by Retrofit
- Retrofit GSON converter
- IconicsTextView and FontAwesome
- RealmIO for offline storage

MVP (VIPER) architecture is followed,  Junit test cases have been written for some of the files.

* Database configuration
- There is no database involved

* Deployment instructions
- Import this project into android studio and set the gradle version to the latest version. Build the project and Run it onto a device. The apk generated will be inside /app/build/outputs/apk/app-debug.apk

### Contribution guidelines ###

* Other guidelines
Please ensure that Java naming standards and also android standards are used.

### Who do I talk to? ###

* Dhara Shah