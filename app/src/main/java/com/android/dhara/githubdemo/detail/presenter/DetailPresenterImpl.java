package com.android.dhara.githubdemo.detail.presenter;

import com.android.dhara.githubdemo.GitHubApp;
import com.android.dhara.githubdemo.detail.model.DetailDataModel;
import com.android.dhara.githubdemo.detail.model.DetailModelAdapterImpl;
import com.android.dhara.githubdemo.detail.router.DetailRouter;
import com.android.dhara.githubdemo.detail.view.DetailView;
import com.android.dhara.githubdemo.di.GitHubInjector;
import com.android.dhara.githubdemo.home.model.RepoInteractor;
import com.android.dhara.githubdemo.home.view.NavigationListener;
import com.android.dhara.githubdemo.home.view.ViewInteractionListener;
import com.android.dhara.githubdemo.network.ServiceError;

import javax.inject.Inject;

public class DetailPresenterImpl implements DetailPresenter, RepoInteractor.ResponseListener, NavigationListener {
    private final DetailView view;
    private final DetailRouter router;
    private final DetailDataModel model;

    @Inject
    RepoInteractor interactor;

    public DetailPresenterImpl(final DetailView view, final DetailRouter router, final DetailDataModel model) {
        this.view = view;
        this.model = model;
        this.router = router;
        GitHubInjector.from(GitHubApp.INSTANCE).inject(this);
    }

    @Override
    public void handleOnViewCreated() {
        view.initViews(new DetailModelAdapterImpl(model, interactor.getGitRepoDetails()), this);
        view.showProgress();
        interactor.fetchSubscribers(model.getGitHubRepo().getFullName(), this);
    }

    @Override
    public void onSuccess() {
        view.hideProgress();
        view.updateData(new DetailModelAdapterImpl(model, interactor.getGitRepoDetails()));
    }

    @Override
    public void onError(final ServiceError error) {
        view.hideProgress();
        view.showError(error.getErrorMessage());
    }

    @Override
    public void onSupportNavigateUp() {
        router.supportNavigationUp();
    }
}
