package com.android.dhara.githubdemo.home.model;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.android.dhara.githubdemo.GitHubApp;
import com.android.dhara.githubdemo.db.RepositoryDB;
import com.android.dhara.githubdemo.detail.model.GitRepoDetails;
import com.android.dhara.githubdemo.di.GitHubInjector;
import com.android.dhara.githubdemo.home.datasource.RepoDataSource;
import com.android.dhara.githubdemo.network.Listener;
import com.android.dhara.githubdemo.network.ServiceError;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

public class RepoInteractorImpl implements RepoInteractor {
    @Inject
    RepoDataSource dataSource;

    private static final int PER_PAGE = 10;

    @VisibleForTesting
    final List<GitHubRepo> repoList = new ArrayList<>();

    private GitRepoDetails gitRepoDetails;

    private long since = 0;
    private boolean isLoadMore = true;

    public RepoInteractorImpl() {
        GitHubInjector.from(GitHubApp.INSTANCE).inject(this);
    }

    @Override
    public void searchRepositories(@NonNull final String searchQuery) {

    }

    @Override
    public void fetchRepositories(final ResponseListener listener) {
        since = RepositoryDB.getSinceRepoValue();
        loadMore(listener);
    }

    @Override
    public void fetchSubscribers(@NonNull final String fullName, final ResponseListener listener) {
        dataSource.getSubscribers(fullName, new Listener<GitRepoDetails>(){
            @Override
            public void onSuccess(final GitRepoDetails repoDetails) {
                gitRepoDetails = repoDetails;
                listener.onSuccess();
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onError(error);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public GitHubRepo getRepo(final int position) {
        return repoList.get(position);
    }

    @Override
    public List<GitHubRepo> getRepoList() {
        return repoList == null || repoList.isEmpty() ? Collections.emptyList() : repoList;
    }

    @Override
    public GitRepoDetails getGitRepoDetails() {
        return gitRepoDetails;
    }

    @Override
    public boolean loadMore() {
        return isLoadMore;
    }

    @Override
    public void loadMore(@NonNull final ResponseListener listener) {
        if (since > 0) {
            fetchFromDB();
            listener.onSuccess();
        }

        getRepositories(listener);

    }

    private void getRepositories(@NonNull final ResponseListener listener) {
        dataSource.getRepositories(since, PER_PAGE, new Listener<Response<List<GitHubRepo>>>() {
            @Override
            public void onSuccess(@NonNull final Response<List<GitHubRepo>> response) {
                if (response.isSuccessful()) {
                    String linkHeader = response.headers().get("Link");
                    String[] linkHeaders = linkHeader.split(",");
                    String[] nextLink = linkHeaders[0].split(";");

                    String nexLinkPath = nextLink[0];
                    String strSince = nexLinkPath.substring(nexLinkPath.indexOf("?") + 1,
                            nexLinkPath.indexOf("&"));

                    since = Integer.parseInt(strSince.split("=")[1]);
                }
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onError(error);
            }

            @Override
            public void onComplete() {
                fetchFromDB();
                listener.onSuccess();
            }
        });
    }

    private void fetchFromDB() {
        repoList.clear();
        repoList.addAll(RepositoryDB.getList());
    }
}
