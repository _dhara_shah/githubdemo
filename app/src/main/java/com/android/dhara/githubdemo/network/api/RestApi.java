package com.android.dhara.githubdemo.network.api;

import com.android.dhara.githubdemo.network.entity.GitHubRepo;
import com.android.dhara.githubdemo.network.entity.RepoOwner;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestApi {
    @GET("/repositories")
    Call<List<GitHubRepo>> getRepositories(@Query("since") long since, @Query("per_page") int perPage);

    @GET("/search/repositories")
    Call<List<GitHubRepo>> searchRepositories(@Query("q") String searchTerm);

    @GET("/repos/{fullName}/subscribers")
    Observable<List<RepoOwner>> getSubscribers(@Path(value = "fullName", encoded = true) String fullName);

    @GET("/repos/{fullName}/forks")
    Observable<List<GitHubRepo>> getForks(@Path(value = "fullName", encoded = true) String fullName);

}