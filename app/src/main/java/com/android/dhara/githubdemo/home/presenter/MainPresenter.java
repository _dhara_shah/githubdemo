package com.android.dhara.githubdemo.home.presenter;

public interface MainPresenter {
    void handleOnViewCreated();
}
