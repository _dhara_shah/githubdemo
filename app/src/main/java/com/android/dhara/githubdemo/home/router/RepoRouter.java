package com.android.dhara.githubdemo.home.router;

import com.android.dhara.githubdemo.network.entity.GitHubRepo;

public interface RepoRouter {
    void openDetail(GitHubRepo gitHubRepo);

    void onBackPressed();

    void loadFragment();
}
