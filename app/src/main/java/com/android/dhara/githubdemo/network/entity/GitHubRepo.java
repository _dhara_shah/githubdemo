package com.android.dhara.githubdemo.network.entity;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

@RealmClass
public class GitHubRepo extends RealmObject implements Serializable {
    @PrimaryKey
    private long id;

    @Required
    @SerializedName("name")
    private String repoName;

    private String description;

    private RepoOwner owner;

    @SerializedName("html_url")
    private String repoUrl;

    private boolean fork;

    public long getId() {
        return id;
    }

    @SerializedName("forks_count")
    private int forksCount;

    @SerializedName("forks_url")
    private String forksUrl;

    @SerializedName("full_name")
    private String fullName;

    @NonNull
    public String getRepoName() {
        return repoName == null ? "" : repoName;
    }

    public String getDescription() {
        return description == null ? "" : description;
    }

    public RepoOwner getOwner() {
        return owner;
    }

    @NonNull
    public String getRepoUrl() {
        return repoUrl == null ? "" : repoUrl;
    }

    public boolean getFork() {
        return fork;
    }

    public int getForksCount() {
        return forksCount;
    }

    public String getForksUrl() {
        return forksUrl;
    }

    public String getFullName() {
        return fullName;
    }
}
