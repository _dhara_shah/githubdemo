package com.android.dhara.githubdemo;

import android.app.Application;
import android.content.Context;

import com.android.dhara.githubdemo.dagger2.components.AppComponent;
import com.android.dhara.githubdemo.dagger2.components.DaggerAppComponent;
import com.android.dhara.githubdemo.dagger2.components.DaggerGitHubComponent;
import com.android.dhara.githubdemo.dagger2.components.GitHubComponent;
import com.android.dhara.githubdemo.dagger2.modules.AppModule;
import com.android.dhara.githubdemo.dagger2.modules.GitHubModuleImpl;
import com.android.dhara.githubdemo.di.GitHubComponentProvider;
import com.android.dhara.githubdemo.di.components.CoreComponent;
import com.android.dhara.githubdemo.di.components.CoreComponentProvider;
import com.android.dhara.githubdemo.di.components.DaggerCoreComponent;
import com.android.dhara.githubdemo.di.modules.CoreModule;
import com.android.dhara.githubdemo.di.modules.NetModule;
import com.android.dhara.githubdemo.network.api.Api;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class GitHubApp extends Application implements CoreComponentProvider, GitHubComponentProvider {
    public static GitHubApp INSTANCE;
    private static AppComponent INJECTOR;
    private static GitHubComponent GITHUB_INJECTOR;
    private static volatile CoreComponent CORE_INJECTOR;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

        Realm.init(INSTANCE);
        final RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        final CoreModule coreModule = new CoreModule(this);
        final NetModule netModule = new NetModule(Api.getHost());

        CORE_INJECTOR = DaggerCoreComponent.builder()
                .coreModule(coreModule)
                .netModule(netModule)
                .build();

        INJECTOR = DaggerAppComponent.builder()
                .coreComponent(CORE_INJECTOR)
                .appModule(new AppModule(this))
                .build();

        GITHUB_INJECTOR = DaggerGitHubComponent.builder()
                .coreComponent(CORE_INJECTOR)
                .gitHubModule(new GitHubModuleImpl())
                .build();

        CORE_INJECTOR.getGitHubLog();
    }

    @Override
    public CoreComponent getCoreComponent() {
        return CORE_INJECTOR;
    }

    @Override
    public GitHubComponent getGitHubComponent(final Context context) {
        return GITHUB_INJECTOR;
    }
}
