package com.android.dhara.githubdemo.home.model;

import android.support.annotation.ColorRes;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public interface RepositoryModelAdapter {
    int VIEW_TYPE_ROW = 1;
    int VIEW_TYPE_LOAD_MORE = 2;

    int getCount();

    @NonNull String getRepoName(int position);

    @NonNull String getRepoDescription(int position);

    @NonNull String getRepoOwnerName(int position);

    @ColorRes
    int getBackgroundColor(int position);

    int getItemViewType(int position);

    boolean loadMore();

    String getAvatar(int position);

    String getId(int position);

    @Visibility
    int getForksVisibility(int position);

    @IntDef({VISIBLE, INVISIBLE, GONE})
    @Retention(RetentionPolicy.SOURCE)
    @interface Visibility {}
}
