package com.android.dhara.githubdemo.home.view;

import android.support.annotation.NonNull;

import com.android.dhara.githubdemo.home.model.RepositoryModelAdapter;

public interface RepoView {
    void initViews(@NonNull RepositoryModelAdapter modelAdapter, @NonNull ViewInteractionListener listener);

    void hideProgress();

    void showProgress();

    void showError(String message);

    void updateAdapter(@NonNull RepositoryModelAdapter modelAdapter);

    void endLoadMore();
}
