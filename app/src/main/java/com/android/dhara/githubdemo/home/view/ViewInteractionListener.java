package com.android.dhara.githubdemo.home.view;

public interface ViewInteractionListener extends NavigationListener {
    void onClick(int position);

    void onLoadMore();
}
