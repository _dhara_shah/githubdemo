package com.android.dhara.githubdemo.dagger2.modules;

import com.android.dhara.githubdemo.home.datasource.RepoDataSource;
import com.android.dhara.githubdemo.home.datasource.RepoDataSourceImpl;
import com.android.dhara.githubdemo.home.model.RepoInteractor;
import com.android.dhara.githubdemo.home.model.RepoInteractorImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class GitHubModule {
    @Provides
    public RepoDataSource provideDataSource() {
        return new RepoDataSourceImpl();
    }

    @Provides
    public RepoInteractor provideRepoInteractor() {
        return new RepoInteractorImpl();
    }
}
