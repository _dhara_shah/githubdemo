package com.android.dhara.githubdemo.db;

import com.android.dhara.githubdemo.network.entity.GitHubRepo;

import java.util.Collection;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public final class RepositoryDB {
    private RepositoryDB() { }

    public static void writeToRealm(final Collection<GitHubRepo> repoList) {
        final Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realmObj -> realmObj.insertOrUpdate(repoList));
    }

    public static List<GitHubRepo> getList() {
        final Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(GitHubRepo.class).findAll());
    }

    public static long getSinceRepoValue() {
        final Realm realm = Realm.getDefaultInstance();

        final RealmResults<GitHubRepo> results = realm.where(GitHubRepo.class)
                .findAllSorted("id", Sort.DESCENDING);
        if (results != null && !results.isEmpty()) {
            final GitHubRepo repo = results.first();
            return repo == null ? 0 : repo.getId();
        }

        return 0;
    }
}