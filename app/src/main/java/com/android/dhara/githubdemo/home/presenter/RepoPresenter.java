package com.android.dhara.githubdemo.home.presenter;

public interface RepoPresenter {
    void handleOnViewCreated();
}
