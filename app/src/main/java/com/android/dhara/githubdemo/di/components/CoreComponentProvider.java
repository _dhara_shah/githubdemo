package com.android.dhara.githubdemo.di.components;

public interface CoreComponentProvider {
    CoreComponent getCoreComponent();
}
