package com.android.dhara.githubdemo.home.presenter;

import com.android.dhara.githubdemo.home.router.RepoRouter;

public class MainPresenterImpl implements MainPresenter {
    private RepoRouter router;

    public MainPresenterImpl(final RepoRouter router) {
        this.router = router;
    }

    @Override
    public void handleOnViewCreated() {
        router.loadFragment();
    }
}
