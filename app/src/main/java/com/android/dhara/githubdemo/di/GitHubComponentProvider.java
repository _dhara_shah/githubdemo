package com.android.dhara.githubdemo.di;

import android.content.Context;

import com.android.dhara.githubdemo.dagger2.components.GitHubComponent;

public interface GitHubComponentProvider {
    GitHubComponent getGitHubComponent(Context context);
}
