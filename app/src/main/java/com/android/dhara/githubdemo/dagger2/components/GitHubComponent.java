package com.android.dhara.githubdemo.dagger2.components;

import com.android.dhara.githubdemo.dagger2.modules.GitHubModule;
import com.android.dhara.githubdemo.detail.presenter.DetailPresenterImpl;
import com.android.dhara.githubdemo.di.ModuleScope;
import com.android.dhara.githubdemo.di.components.CoreComponent;
import com.android.dhara.githubdemo.home.datasource.RepoDataSourceImpl;
import com.android.dhara.githubdemo.home.model.RepoInteractorImpl;
import com.android.dhara.githubdemo.home.presenter.RepoPresenterImpl;

import dagger.Component;

@ModuleScope
@Component(modules = GitHubModule.class, dependencies = CoreComponent.class)
public interface GitHubComponent {
    void inject(RepoPresenterImpl repoPresenter);

    void inject(RepoDataSourceImpl repoDataSource);

    void inject(RepoInteractorImpl repoInteractor);

    void inject(DetailPresenterImpl detailPresenter);

}