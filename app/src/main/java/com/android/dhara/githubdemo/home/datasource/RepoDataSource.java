package com.android.dhara.githubdemo.home.datasource;

import android.support.annotation.NonNull;

import com.android.dhara.githubdemo.detail.model.GitRepoDetails;
import com.android.dhara.githubdemo.network.Listener;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;
import com.android.dhara.githubdemo.network.entity.RepoOwner;

import java.util.List;

import retrofit2.Response;

public interface RepoDataSource {
    void getRepositories(long since, int perPage, @NonNull Listener<Response<List<GitHubRepo>>> listener);

    void getSubscribers(@NonNull String repoName, @NonNull Listener<GitRepoDetails> listener);
}
