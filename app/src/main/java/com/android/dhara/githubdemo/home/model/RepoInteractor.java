package com.android.dhara.githubdemo.home.model;

import android.support.annotation.NonNull;

import com.android.dhara.githubdemo.detail.model.DetailDataModel;
import com.android.dhara.githubdemo.detail.model.GitRepoDetails;
import com.android.dhara.githubdemo.network.ServiceError;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;
import com.android.dhara.githubdemo.network.entity.RepoOwner;

import java.util.List;

public interface RepoInteractor {
    void searchRepositories(@NonNull String searchQuery);

    void fetchRepositories(ResponseListener listener);

    void fetchSubscribers(@NonNull String fullName, ResponseListener listener);

    GitHubRepo getRepo(int position);

    List<GitHubRepo> getRepoList();

    GitRepoDetails getGitRepoDetails();

    boolean loadMore();

    void loadMore(@NonNull ResponseListener listener);

    interface ResponseListener {
        void onSuccess();

        void onError(ServiceError error);
    }
}
