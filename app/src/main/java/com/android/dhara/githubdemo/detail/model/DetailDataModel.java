package com.android.dhara.githubdemo.detail.model;

import android.support.annotation.NonNull;

import com.android.dhara.githubdemo.network.entity.GitHubRepo;

public class DetailDataModel {
    private GitHubRepo gitHubRepo;

    public DetailDataModel(final GitHubRepo repo) {
        gitHubRepo = repo;
    }

    public GitHubRepo getGitHubRepo() {
        return gitHubRepo;
    }

    static class Builder {
        private GitHubRepo repo;

        public Builder(@NonNull final GitHubRepo repo) {
            this.repo = repo;
        }

        public DetailDataModel build() {
            return new DetailDataModel(repo);
        }
    }
}
