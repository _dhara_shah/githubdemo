package com.android.dhara.githubdemo.home.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.home.presenter.RepoPresenter;
import com.android.dhara.githubdemo.home.presenter.RepoPresenterImpl;
import com.android.dhara.githubdemo.home.router.RepoRouter;
import com.android.dhara.githubdemo.home.router.RepoRouterImpl;

public class RepoFragment extends Fragment {
    public static RepoFragment newInstance() {
        return new RepoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final RepoRouter router = new RepoRouterImpl(getActivity());
        final RepoView repoView = new RepoViewImpl(getActivity());
        final RepoPresenter presenter = new RepoPresenterImpl(repoView, router);
        presenter.handleOnViewCreated();
    }
}
