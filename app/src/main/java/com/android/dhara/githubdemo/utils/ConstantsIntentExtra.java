package com.android.dhara.githubdemo.utils;

public class ConstantsIntentExtra {
    public static final String EXTRA_REPOSITORY = "EXTRA_REPOSITORY";
    public static final String EXTRA_BUNDLE = "EXTRA_BUNDLE";
}
