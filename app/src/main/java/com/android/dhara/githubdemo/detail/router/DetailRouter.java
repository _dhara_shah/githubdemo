package com.android.dhara.githubdemo.detail.router;

public interface DetailRouter {
    void supportNavigationUp();
}
