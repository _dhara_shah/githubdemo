package com.android.dhara.githubdemo.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.dhara.githubdemo.dagger2.components.GitHubComponent;

public final class GitHubInjector {
    private GitHubInjector() { }

    public static GitHubComponent from(@NonNull final Context context) {
        final GitHubComponentProvider provider = (GitHubComponentProvider) context.getApplicationContext();
        return provider.getGitHubComponent(context);
    }
}
