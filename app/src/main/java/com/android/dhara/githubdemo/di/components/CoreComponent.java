package com.android.dhara.githubdemo.di.components;

import com.android.dhara.githubdemo.di.modules.CoreModule;
import com.android.dhara.githubdemo.di.modules.NetModule;
import com.android.dhara.githubdemo.utils.GitHubLog;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {CoreModule.class, NetModule.class})
public interface CoreComponent {
    GitHubLog getGitHubLog();

    Retrofit getRetrofit();

    Gson getGson();

    OkHttpClient getOkHttpClient();

    void inject(GitHubLog githubLog);
}
