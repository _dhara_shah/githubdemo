package com.android.dhara.githubdemo.home.view;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.home.model.RepositoryModelAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

final class RepositoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final RepositoryModelAdapter modelAdapter;
    private static ViewInteractionListener listener;

    public static RepositoryAdapter create(@NonNull final RepositoryModelAdapter modelAdapter,
                                           @NonNull final ViewInteractionListener listener) {
        return new RepositoryAdapter(modelAdapter, listener);
    }

    private RepositoryAdapter(@NonNull final RepositoryModelAdapter modelAdapter,
                              @NonNull final ViewInteractionListener viewListener) {
        this.modelAdapter = modelAdapter;
        listener = viewListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final RecyclerView.ViewHolder vh;
        if (viewType == RepositoryModelAdapter.VIEW_TYPE_ROW) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_github_repo_individual, parent, false);
            vh = new ViewHolder(itemView);
        } else {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_progress_bar, parent, false);
            vh = new ProgressViewHolder(itemView);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder vh, final int position) {
        final int viewType = vh.getItemViewType();
        if (viewType == RepositoryModelAdapter.VIEW_TYPE_LOAD_MORE) {
            final ProgressViewHolder holder = (ProgressViewHolder) vh;
            holder.progressView.setVisibility(modelAdapter.loadMore() ? View.VISIBLE : View.GONE);
        } else {
            final ViewHolder holder = (ViewHolder) vh;
            final int pos = getNormalizedPosition(position);
            holder.txtRepoName.setText(modelAdapter.getRepoName(pos));
            holder.txtRepoDescription.setText(modelAdapter.getRepoDescription(pos));
            holder.txtOwnerName.setText(modelAdapter.getRepoOwnerName(pos));
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(),
                    modelAdapter.getBackgroundColor(pos)));
            holder.txtForks.setText(holder.itemView.getContext().getString(R.string.icon_forks_only));
            holder.txtForks.setVisibility(modelAdapter.getForksVisibility(position));
            Glide.with(vh.itemView.getContext())
                    .load(modelAdapter.getAvatar(position))
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.imgAvatar);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.itemView.setTransitionName(modelAdapter.getId(position));
            }
        }
    }

    @Override
    public int getItemCount() {
        return modelAdapter.getCount();
    }

    @Override
    public int getItemViewType(final int position) {
        return modelAdapter.getItemViewType(position);
    }

    private int getNormalizedPosition(final int position) {
        if (position >= getItemCount()) {
            return position - 1;
        }
        return position;
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtRepoName;
        TextView txtRepoDescription;
        TextView txtOwnerName;
        TextView txtForks;
        ImageView imgAvatar;

        ViewHolder(final View itemView) {
            super(itemView);
            txtRepoName = itemView.findViewById(R.id.txt_repo_name);
            txtRepoDescription =   itemView.findViewById(R.id.txt_description);
            txtOwnerName =   itemView.findViewById(R.id.txt_owner_login);
            txtForks = itemView.findViewById(R.id.txt_forks);
            imgAvatar = itemView.findViewById(R.id.img_avatar);

            itemView.setOnClickListener((view) -> {
                final int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    listener.onClick(position);
                }
            });
        }
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        View progressView;
        ProgressViewHolder(final View itemView) {
            super(itemView);
            progressView  = itemView.findViewById(R.id.progress_bar);
        }
    }
}
