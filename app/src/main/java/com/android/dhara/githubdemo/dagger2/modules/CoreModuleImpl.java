package com.android.dhara.githubdemo.dagger2.modules;

import android.content.Context;

import com.android.dhara.githubdemo.di.modules.CoreModule;

public class CoreModuleImpl extends CoreModule {
    public CoreModuleImpl(Context context) {
        super(context);
    }
}