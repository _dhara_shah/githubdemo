package com.android.dhara.githubdemo.dagger2.components;

import com.android.dhara.githubdemo.dagger2.modules.AppModule;
import com.android.dhara.githubdemo.di.ModuleScope;
import com.android.dhara.githubdemo.di.components.CoreComponent;

import dagger.Component;

@ModuleScope
@Component(modules = AppModule.class, dependencies = CoreComponent.class)
public interface AppComponent {

}
