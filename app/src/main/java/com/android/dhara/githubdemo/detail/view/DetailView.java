package com.android.dhara.githubdemo.detail.view;

import android.support.annotation.NonNull;

import com.android.dhara.githubdemo.detail.model.DetailModelAdapter;
import com.android.dhara.githubdemo.home.view.NavigationListener;
import com.android.dhara.githubdemo.home.view.ViewInteractionListener;

public interface DetailView {
    void initViews(@NonNull DetailModelAdapter modelAdapter, @NonNull NavigationListener listener);

    void showProgress();

    void hideProgress();

    void showError(String errorMessage);

    void updateData(@NonNull DetailModelAdapter modelAdapter);
}
