package com.android.dhara.githubdemo.home.model;

import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.view.View;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;

import java.util.List;

public class RepositoryModelAdapterImpl implements RepositoryModelAdapter {
    private final List<GitHubRepo> repoList;
    private final boolean isLoadMore;

    public static RepositoryModelAdapterImpl from(@NonNull final List<GitHubRepo> repoList, final boolean isLoadMore) {
        return new RepositoryModelAdapterImpl(repoList, isLoadMore);
    }

    private RepositoryModelAdapterImpl(@NonNull final List<GitHubRepo> repoList, final boolean isLoadMore) {
        this.repoList = repoList;
        this.isLoadMore = isLoadMore;
    }

    @Override
    public int getCount() {
        if (repoList.isEmpty()) {
            return 0;
        }
        return repoList.size() + 1;
    }

    @NonNull
    @Override
    public String getRepoName(final int position) {
        return repoList.get(position).getRepoName();
    }

    @NonNull
    @Override
    public String getRepoDescription(final int position) {
        return repoList.get(position).getDescription();
    }

    @NonNull
    @Override
    public String getRepoOwnerName(final int position) {
        return repoList.get(position).getOwner().getOwnerName();
    }

    @ColorRes
    @Override
    public int getBackgroundColor(final int position) {
        if (hasFork(position)) {
            return R.color.colorGreen;
        }
        return R.color.colorWhite;
    }

    @Override
    public int getItemViewType(final int position) {
        if (position >= repoList.size()) {
            return VIEW_TYPE_LOAD_MORE;
        }
        return VIEW_TYPE_ROW;
    }

    @Override
    public boolean loadMore() {
        return isLoadMore;
    }

    @Override
    public String getAvatar(final int position) {
        return repoList.get(position).getOwner().getAvatarUrl();
    }

    @Override
    public String getId(final int position) {
        return String.valueOf(repoList.get(position).getId());
    }

    @Visibility
    @Override
    public int getForksVisibility(final int position) {
        return hasFork(position) ? View.VISIBLE : View.GONE;
    }

    private boolean hasFork(final int position) {
        return repoList.get(position).getFork();
    }
}
