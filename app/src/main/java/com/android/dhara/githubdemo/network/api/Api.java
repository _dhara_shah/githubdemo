package com.android.dhara.githubdemo.network.api;

public class Api {
    private static final String BASE_URL = "https://api.github.com/";

    public static String getHost() {
        return BASE_URL;
    }
}
