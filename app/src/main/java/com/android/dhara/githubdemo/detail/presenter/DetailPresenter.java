package com.android.dhara.githubdemo.detail.presenter;

public interface DetailPresenter {
    void handleOnViewCreated();
}
