package com.android.dhara.githubdemo.detail.view;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.detail.model.DetailModelAdapter;
import com.android.dhara.githubdemo.home.view.NavigationListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mikepenz.iconics.view.IconicsTextView;

public class DetailViewImpl implements DetailView {
    private static final int SPAN_COUNT = 3;
    private AppCompatActivity activity;
    private View progressView;
    private RecyclerView rvSubscriberList;
    private SubscribersAdapter adapter;
    private NavigationListener listener;

    public DetailViewImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(@NonNull final DetailModelAdapter modelAdapter,
                          @NonNull final NavigationListener listener) {
        this.listener = listener;

        setUpToolbar();

        final ViewStub progressStub = activity.findViewById(R.id.progress_stub);
        progressView = progressStub.inflate();

        final GridLayoutManager lm = new GridLayoutManager(activity, SPAN_COUNT);
        rvSubscriberList = activity.findViewById(R.id.rv_subscriber_list);
        rvSubscriberList.setLayoutManager(lm);
        rvSubscriberList.setHasFixedSize(true);

        final RecyclerView.ItemAnimator animator = rvSubscriberList.getItemAnimator();
        animator.setAddDuration(200);
        animator.setRemoveDuration(200);


        adapter = new SubscribersAdapter(modelAdapter);
        rvSubscriberList.setAdapter(adapter);

        final TextView txtUser = activity.findViewById(R.id.txt_repo_owner);
        txtUser.setText(modelAdapter.getOwnerName());

        final ImageView imgAvatar = activity.findViewById(R.id.img_avatar);
        Glide.with(activity)
                .load(modelAdapter.getAvatarUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(imgAvatar);

        final TextView txtDesc = activity.findViewById(R.id.txt_description);
        txtDesc.setText(modelAdapter.getRepoDescription());

        final TextView txtRepoName = activity.findViewById(R.id.txt_repo_name);
        txtRepoName.setText(modelAdapter.getRepoName());
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void showError(final String errorMessage) {
        Snackbar.make(rvSubscriberList, errorMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void updateData(@NonNull final DetailModelAdapter modelAdapter) {
        adapter = new SubscribersAdapter(modelAdapter);
        rvSubscriberList.swapAdapter(adapter, false);

        final IconicsTextView txtSubscribers = activity.findViewById(R.id.icon_subscribers);
        txtSubscribers.setText(activity.getString(R.string.icon_subscribers, modelAdapter.getCount()));

        final IconicsTextView txtForks = activity.findViewById(R.id.icon_forks);
        txtForks.setText(activity.getString(R.string.icon_forks, modelAdapter.getForks()));
    }

    private void setUpToolbar() {
        final Toolbar toolbar = activity.findViewById(R.id.toolbar_stub);
        activity.setSupportActionBar(toolbar);

        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setHomeButtonEnabled(true);
        }

        toolbar.setNavigationOnClickListener(v -> listener.onSupportNavigateUp());

        toolbar.setAlpha(0);
        toolbar.setTranslationY(-300);

        toolbar.animate().setDuration(1000).translationY(0).alpha(1);

        for(int i = 0; i < toolbar.getChildCount(); i++ ){
            final View view = toolbar.getChildAt(i);
            view.setTranslationY(-300);
            view.animate().setStartDelay(900).setDuration(1000).translationY(0);
        }
    }
}
