package com.android.dhara.githubdemo.detail.model;

import com.android.dhara.githubdemo.network.entity.RepoOwner;

import java.util.Collections;
import java.util.List;

public class DetailModelAdapterImpl implements DetailModelAdapter {
    private DetailDataModel dataModel;
    private GitRepoDetails gitRepoDetails;

    public DetailModelAdapterImpl(final DetailDataModel dataModel, final GitRepoDetails gitRepoDetails) {
        this.dataModel = dataModel;
        this.gitRepoDetails = gitRepoDetails;
    }

    @Override
    public String getAvatarUrl() {
        return dataModel.getGitHubRepo().getOwner().getAvatarUrl();
    }

    @Override
    public int getForks() {
        return gitRepoDetails.getForkCount();
    }

    @Override
    public List<RepoOwner> getSubscribers() {
        return isEmpty() ? Collections.emptyList() : gitRepoDetails.getSubscriberList();
    }

    @Override
    public String getRepoDescription() {
        return dataModel.getGitHubRepo().getDescription();
    }

    @Override
    public String getOwnerName() {
        return dataModel.getGitHubRepo().getOwner().getOwnerName();
    }

    @Override
    public int getCount() {
        return isEmpty() ? 0 : gitRepoDetails.getSubscriberList().size();
    }

    @Override
    public String getRepoOwnerName(final int position) {
        return getSubscribers().get(position).getOwnerName();
    }

    @Override
    public String getRepoOwnerAvatar(final int position) {
        return getSubscribers().get(position).getAvatarUrl();
    }

    @Override
    public String getRepoName() {
        return dataModel.getGitHubRepo().getRepoName();
    }

    private boolean isEmpty() {
        return gitRepoDetails == null ||
                gitRepoDetails.getSubscriberList() == null ||
                gitRepoDetails.getSubscriberList().isEmpty();
    }
}
