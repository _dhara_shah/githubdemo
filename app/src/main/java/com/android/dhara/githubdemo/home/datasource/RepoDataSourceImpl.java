package com.android.dhara.githubdemo.home.datasource;

import android.support.annotation.NonNull;

import com.android.dhara.githubdemo.GitHubApp;
import com.android.dhara.githubdemo.db.RepositoryDB;
import com.android.dhara.githubdemo.detail.model.GitRepoDetails;
import com.android.dhara.githubdemo.di.GitHubInjector;
import com.android.dhara.githubdemo.network.Listener;
import com.android.dhara.githubdemo.network.ServiceError;
import com.android.dhara.githubdemo.network.api.RestApi;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;
import com.android.dhara.githubdemo.network.entity.RepoOwner;
import com.android.dhara.githubdemo.utils.GitHubLog;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RepoDataSourceImpl implements RepoDataSource {
    private static final String TAG = RepoDataSourceImpl.class.getSimpleName();
    @Inject
    Retrofit retrofit;

    @Inject
    GitHubLog logger;

    public RepoDataSourceImpl() {
        GitHubInjector.from(GitHubApp.INSTANCE).inject(this);
    }

    @Override
    public void getRepositories(final long since, final int perPage, @NonNull final Listener<Response<List<GitHubRepo>>> listener) {
        retrofit.create(RestApi.class).getRepositories(since, perPage)
                .enqueue(new Callback<List<GitHubRepo>>() {
                    @Override
                    public void onResponse(@NonNull final Call<List<GitHubRepo>> call, @NonNull final Response<List<GitHubRepo>> response) {
                        RepositoryDB.writeToRealm(response.body());
                        listener.onSuccess(response);
                        listener.onComplete();
                    }

                    @Override
                    public void onFailure(@NonNull final Call<List<GitHubRepo>> call, @NonNull final Throwable t) {
                        logger.wtf(TAG, t);
                        listener.onError(new ServiceError(t.getMessage()));
                    }
                });

    }

    @Override
    public void getSubscribers(@NonNull final String fullName, @NonNull final Listener<GitRepoDetails> listener) {
        final Observable<List<RepoOwner>> ownersObservable = retrofit.create(RestApi.class).getSubscribers(fullName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> {
                    logger.wtf(TAG, throwable);
                    listener.onError(new ServiceError(throwable.getMessage()));
                });

        final Observable<List<GitHubRepo>> forkObservable = retrofit.create(RestApi.class).getForks(fullName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> {
                    logger.wtf(TAG, throwable);
                    listener.onError(new ServiceError(throwable.getMessage()));
                });

        final Observable<GitRepoDetails> gitResult = Observable.zip(ownersObservable,
                forkObservable, GitRepoDetails::new);
        gitResult.subscribe(listener::onSuccess);
    }
}
