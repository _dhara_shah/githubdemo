package com.android.dhara.githubdemo.dagger2.modules;

import android.app.Application;

import com.android.dhara.githubdemo.GitHubApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final GitHubApp application;

    public AppModule(final GitHubApp application) {
        this.application = application;
    }

    @Singleton
    @Provides
    public Application provideApplication() {
        return application;
    }
}
