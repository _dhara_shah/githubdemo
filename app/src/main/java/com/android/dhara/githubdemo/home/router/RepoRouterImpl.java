package com.android.dhara.githubdemo.home.router;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.detail.view.DetailActivity;
import com.android.dhara.githubdemo.home.view.RepoFragment;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;
import com.android.dhara.githubdemo.utils.ConstantsIntentExtra;

public class RepoRouterImpl implements RepoRouter {
    private final FragmentActivity activity;

    public RepoRouterImpl(final FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void openDetail(final GitHubRepo gitHubRepo) {
        final Bundle bundle = new Bundle();
        bundle.putSerializable(ConstantsIntentExtra.EXTRA_REPOSITORY, gitHubRepo);
        DetailActivity.startActivity(activity, bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setEnterTransition(new Fade());
            activity.getWindow().setExitTransition(new Fade());
        }
    }

    @Override
    public void onBackPressed() {
        activity.onBackPressed();
    }

    @Override
    public void loadFragment() {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_repo_list, RepoFragment.newInstance());
        ft.commit();
    }
}
