package com.android.dhara.githubdemo.detail.view;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Window;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.detail.model.DetailDataModel;
import com.android.dhara.githubdemo.detail.model.DetailModelCreator;
import com.android.dhara.githubdemo.detail.presenter.DetailPresenter;
import com.android.dhara.githubdemo.detail.presenter.DetailPresenterImpl;
import com.android.dhara.githubdemo.detail.router.DetailRouter;
import com.android.dhara.githubdemo.detail.router.DetailRouterImpl;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;
import com.android.dhara.githubdemo.utils.ConstantsIntentExtra;

public class DetailActivity extends AppCompatActivity {

    public static void startActivity(final Context context,
                                     final Bundle bundle) {
        final Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtras(bundle);
        ActivityCompat.startActivity(context, intent, null);
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
            getWindow().setEnterTransition(new Slide());
            getWindow().setExitTransition(new Explode());
        }

        setContentView(R.layout.activity_detail);

        DetailDataModel model = null;
        if (getIntent().getExtras() != null) {
            final GitHubRepo repo =
                    (GitHubRepo) getIntent().getSerializableExtra(ConstantsIntentExtra.EXTRA_REPOSITORY);
            model = DetailModelCreator.create(repo);
        }

        final DetailRouter router = new DetailRouterImpl(this);
        final DetailView view = new DetailViewImpl(this);
        final DetailPresenter presenter = new DetailPresenterImpl(view, router, model);
        presenter.handleOnViewCreated();
    }
}
