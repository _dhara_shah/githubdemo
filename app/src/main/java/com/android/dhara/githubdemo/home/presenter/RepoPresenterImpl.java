package com.android.dhara.githubdemo.home.presenter;

import com.android.dhara.githubdemo.GitHubApp;
import com.android.dhara.githubdemo.di.GitHubInjector;
import com.android.dhara.githubdemo.home.model.RepoInteractor;
import com.android.dhara.githubdemo.home.model.RepositoryModelAdapterImpl;
import com.android.dhara.githubdemo.home.router.RepoRouter;
import com.android.dhara.githubdemo.home.view.RepoView;
import com.android.dhara.githubdemo.home.view.ViewInteractionListener;
import com.android.dhara.githubdemo.network.ServiceError;

import javax.inject.Inject;

public class RepoPresenterImpl implements RepoPresenter, ViewInteractionListener, RepoInteractor.ResponseListener {
    @Inject
    RepoInteractor interactor;

    private RepoView repoView;
    private RepoRouter router;

    public RepoPresenterImpl(final RepoView repoView, final RepoRouter router) {
        this.repoView = repoView;
        this.router = router;
        GitHubInjector.from(GitHubApp.INSTANCE).inject(this);
    }

    @Override
    public void handleOnViewCreated() {
        repoView.initViews(RepositoryModelAdapterImpl.from(interactor.getRepoList(), interactor.loadMore()), this);
        repoView.showProgress();
        interactor.fetchRepositories(this);
    }

    @Override
    public void onClick(final int position) {
        router.openDetail(interactor.getRepo(position));
    }

    @Override
    public void onLoadMore() {
        if (interactor.loadMore()) {
            interactor.loadMore(this);
            return;
        }
        repoView.endLoadMore();
    }

    @Override
    public void onSuccess() {
        repoView.hideProgress();
        repoView.updateAdapter(RepositoryModelAdapterImpl.from(interactor.getRepoList(), interactor.loadMore()));
    }

    @Override
    public void onError(final ServiceError error) {
        repoView.hideProgress();
        repoView.showError(error.getErrorMessage());
    }

    @Override
    public void onSupportNavigateUp() {
        router.onBackPressed();
    }
}
