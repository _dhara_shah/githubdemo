package com.android.dhara.githubdemo.di.modules;

import android.app.Application;
import android.content.Context;

import com.android.dhara.githubdemo.utils.GitHubLog;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CoreModule {
    private final Context context;

    public CoreModule(final Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    public GitHubLog provideXingGithubLog() {
        return new GitHubLog();
    }

    @Singleton
    @Provides
    Application providesApplication() {
        return (Application) context;
    }
}
