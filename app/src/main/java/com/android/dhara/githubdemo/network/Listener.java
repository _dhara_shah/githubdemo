package com.android.dhara.githubdemo.network;

public interface Listener<T> {
    void onSuccess(T response);

    void onError(ServiceError error);

    void onComplete();
}
