package com.android.dhara.githubdemo.home.view;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewStub;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.home.model.RepositoryModelAdapter;

public class RepoViewImpl implements RepoView {
    private final int visibleThreshold = 5;
    private boolean canLoadMore = true;
    private final FragmentActivity activity;
    private ViewInteractionListener listener;
    private View progressView;
    private RecyclerView rvRepoList;
    private int totalItemCount;
    private int lastVisibleItem;
    private boolean isLoading;
    private RepositoryAdapter adapter;

    public RepoViewImpl(final FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(@NonNull final RepositoryModelAdapter modelAdapter,
                          @NonNull final ViewInteractionListener listener) {
        this.listener = listener;

        setUpToolbar();

        progressView = activity.findViewById(R.id.lnr_progress_view);

        rvRepoList = activity.findViewById(R.id.rv_repo_list);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(activity.getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvRepoList.setLayoutManager(layoutManager);

        final RecyclerView.ItemAnimator animator = rvRepoList.getItemAnimator();
        animator.setAddDuration(200);
        animator.setRemoveDuration(200);rvRepoList.setItemAnimator(new DefaultItemAnimator());

        final int dividerSpace = (int) activity.getResources().getDimension(R.dimen.margin_small);
        rvRepoList.addItemDecoration(new CustomItemDecoration(dividerSpace));

        rvRepoList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(final RecyclerView recyclerView, final int dx, final int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (canLoadMore) {
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        listener.onLoadMore();
                        isLoading = true;
                    }
                }
            }
        });

        adapter = RepositoryAdapter.create(modelAdapter, listener);
        rvRepoList.setAdapter(adapter);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(final String message) {
        Snackbar.make(rvRepoList, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void updateAdapter(@NonNull final RepositoryModelAdapter modelAdapter) {
        adapter = RepositoryAdapter.create(modelAdapter, listener);
        rvRepoList.swapAdapter(adapter, false);
        isLoading = false;
    }

    @Override
    public void endLoadMore() {
        canLoadMore = false;
    }

    private void setUpToolbar() {
        final ViewStub toolbarStub = activity.findViewById(R.id.toolbar_stub);
        final Toolbar toolbar = (Toolbar) toolbarStub.inflate();
        ((AppCompatActivity) activity).setSupportActionBar(toolbar);

        toolbar.setTitle(R.string.app_name);

        toolbar.setAlpha(0);
        toolbar.setTranslationY(-300);

        toolbar.animate().setDuration(1000).translationY(0).alpha(1);

        for(int i = 0; i < toolbar.getChildCount(); i++ ){
            final View view = toolbar.getChildAt(i);
            view.setTranslationY(-300);
            view.animate().setStartDelay(900).setDuration(1000).translationY(0);
        }
    }
}
