package com.android.dhara.githubdemo.detail.view;

import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.detail.model.DetailModelAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class SubscribersAdapter  extends RecyclerView.Adapter<SubscribersAdapter.ViewHolder> {
    private final DetailModelAdapter modelAdapter;

    public SubscribersAdapter(final DetailModelAdapter modelAdapter) {
        this.modelAdapter = modelAdapter;
    }

    @Override
    public SubscribersAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_subscriber_individual, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SubscribersAdapter.ViewHolder holder, final int position) {
        holder.txtUserName.setText(modelAdapter.getRepoOwnerName(position));
        Glide.with(holder.itemView.getContext())
                .load(modelAdapter.getRepoOwnerAvatar(position))
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imgAvatar);
    }

    @Override
    public int getItemCount() {
        return modelAdapter.getCount();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtUserName;
        ImageView imgAvatar;

        ViewHolder(final View itemView) {
            super(itemView);
            txtUserName = itemView.findViewById(R.id.txt_owner_login);
            imgAvatar = itemView.findViewById(R.id.img_avatar);
        }
    }
}