package com.android.dhara.githubdemo.detail.model;

import com.android.dhara.githubdemo.network.entity.RepoOwner;

import java.util.List;

public interface DetailModelAdapter {
    String getAvatarUrl();

    int getForks();

    List<RepoOwner> getSubscribers();

    String getRepoDescription();

    String getOwnerName();

    int getCount();

    String getRepoOwnerName(int position);

    String getRepoOwnerAvatar(int position);

    String getRepoName();
}
