package com.android.dhara.githubdemo.network.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

@RealmClass
public class RepoOwner extends RealmObject implements Serializable {
    @PrimaryKey
    private long id;

    @Required
    @SerializedName("login")
    private String ownerName;

    @Required
    @SerializedName("html_url")
    private String ownerUrl;

    @Required
    @SerializedName("avatar_url")
    private String avatarUrl;

    public long getId() {
        return id;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getOwnerUrl() {
        return ownerUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
