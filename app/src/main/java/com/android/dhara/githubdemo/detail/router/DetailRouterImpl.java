package com.android.dhara.githubdemo.detail.router;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;

public class DetailRouterImpl implements DetailRouter {
    private final AppCompatActivity activity;

    public DetailRouterImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void supportNavigationUp() {
        activity.onBackPressed();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setEnterTransition(new Explode());
            activity.getWindow().setExitTransition(new Explode());
        }
    }
}
