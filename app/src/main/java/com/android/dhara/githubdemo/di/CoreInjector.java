package com.android.dhara.githubdemo.di;

import android.content.Context;

import com.android.dhara.githubdemo.di.components.CoreComponent;
import com.android.dhara.githubdemo.di.components.CoreComponentProvider;

public final class CoreInjector {
    private CoreInjector() { }

    public static CoreComponent from(final Context context) {
        final CoreComponentProvider provider = (CoreComponentProvider) context.getApplicationContext();
        return provider.getCoreComponent();
    }
}
