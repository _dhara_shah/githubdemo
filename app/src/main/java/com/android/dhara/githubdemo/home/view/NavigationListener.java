package com.android.dhara.githubdemo.home.view;

public interface NavigationListener {
    void onSupportNavigateUp();
}
