package com.android.dhara.githubdemo.detail.model;

import com.android.dhara.githubdemo.network.entity.GitHubRepo;
import com.android.dhara.githubdemo.network.entity.RepoOwner;

import java.util.List;

public class GitRepoDetails {
    private final List<RepoOwner> ownerList;
    private final List<GitHubRepo> forkList;

    public GitRepoDetails(final List<RepoOwner> ownerList, final List<GitHubRepo> forkList) {
        this.ownerList = ownerList;
        this.forkList = forkList;
    }

    public List<RepoOwner> getSubscriberList() {
        return ownerList;
    }

    public int getForkCount() {
        return forkList.size();
    }
}
