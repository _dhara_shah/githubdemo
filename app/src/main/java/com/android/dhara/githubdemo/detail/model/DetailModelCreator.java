package com.android.dhara.githubdemo.detail.model;

import android.support.annotation.NonNull;

import com.android.dhara.githubdemo.network.entity.GitHubRepo;

public final class DetailModelCreator {
    @NonNull
    public static DetailDataModel create(@NonNull final GitHubRepo repo) {
        return new DetailDataModel.Builder(repo)
                .build();
    }
}
