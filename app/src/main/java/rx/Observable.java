package rx;

/**
 * This is a dummy Observable class inside the package rx
 * to allow junit testing of realm objects
 * For more information, visit:  https://github.com/realm/realm-java/issues/3478
 */
public class Observable {
}
