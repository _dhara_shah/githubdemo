package com.android.dhara.githubdemo.home.model;

import com.android.dhara.githubdemo.GitHubBaseTest;
import com.android.dhara.githubdemo.home.datasource.RepoDataSource;
import com.android.dhara.githubdemo.network.Listener;
import com.android.dhara.githubdemo.network.ServiceError;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Headers;
import retrofit2.Response;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@PrepareForTest({Response.class, Headers.class})
@RunWith(PowerMockRunner.class)
public class RepoInteractorImplTest extends GitHubBaseTest {
    private static final int POSITION = 0;
    private static final String LINK_HEADER = "Link";
    private static final String LINKS = "link1?since=1&per_page=100;link2, link3;link4";

    private RepoInteractorImpl interactor;

    @Mock
    RepoDataSource dataSource;
    @Mock
    RepoInteractor.ResponseListener listener;
    @Captor
    ArgumentCaptor<Listener<Response<List<GitHubRepo>>>> responseListener;
    @Mock
    Response<List<GitHubRepo>> response;
    @Mock
    ServiceError error;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        PowerMockito.mockStatic(Response.class, Headers.class);
        when(gitHubModule.provideDataSource()).thenReturn(dataSource);
        interactor = new RepoInteractorImpl();
    }

    @Test
    public void testFetchRepositoriesSuccess() {
        final Headers headers = mock(Headers.class);
        when(response.isSuccessful()).thenReturn(true);
        when(response.body()).thenReturn(interactor.repoList);
        when(response.headers()).thenReturn(headers);
        when(headers.get(LINK_HEADER)).thenReturn(LINKS);
        interactor.fetchRepositories(listener);
        verify(dataSource).getRepositories(eq(1), eq(10), responseListener.capture());
        responseListener.getValue().onSuccess(response);
        verify(listener).onSuccess();
    }

    @Test
    public void testFetchRepositoriesError() {
        interactor.fetchRepositories(listener);
        verify(dataSource).getRepositories(eq(1), eq(10), responseListener.capture());
        responseListener.getValue().onError(error);
        verify(listener).onError(eq(error));
    }

    @Test
    public void testGetRepoList() {
        when(response.body()).thenReturn(interactor.repoList);
        interactor.getRepoList();
        assertEquals(interactor.getRepoList(), response.body());
    }

    @Test
    public void testLoadMore() {
        assertEquals(interactor.loadMore(), true);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(dataSource, listener, error);
    }
}
