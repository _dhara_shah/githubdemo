package com.android.dhara.githubdemo;

import android.content.Context;
import android.support.annotation.CallSuper;

import com.android.dhara.githubdemo.dagger2.components.DaggerGitHubComponent;
import com.android.dhara.githubdemo.dagger2.components.GitHubComponent;
import com.android.dhara.githubdemo.dagger2.modules.GitHubModule;
import com.android.dhara.githubdemo.di.GitHubInjector;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;

@RunWith(PowerMockRunner.class)
@PrepareForTest(GitHubInjector.class)
public class GitHubBaseTest extends CoreBaseTest {
    @Mock
    protected GitHubModule gitHubModule;

    @Override
    @CallSuper
    public void setUp() throws Exception {
        super.setUp();

        final GitHubComponent gitHubComponent = DaggerGitHubComponent
                .builder()
                .coreComponent(coreComponent)
                .gitHubModule(gitHubModule)
                .build();
        PowerMockito.mockStatic(GitHubInjector.class);
        PowerMockito.when(GitHubInjector.from(any(Context.class))).thenReturn(gitHubComponent);
    }
}
