package com.android.dhara.githubdemo;

import android.app.Application;
import android.content.Context;
import android.support.annotation.CallSuper;

import com.android.dhara.githubdemo.di.CoreInjector;
import com.android.dhara.githubdemo.di.components.CoreComponent;
import com.android.dhara.githubdemo.di.components.DaggerCoreComponent;
import com.android.dhara.githubdemo.di.modules.CoreModule;
import com.android.dhara.githubdemo.di.modules.NetModule;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CoreInjector.class, Retrofit.class, Cache.class})
public class CoreBaseTest {
    @Mock
    protected NetModule netModule;
    @Mock
    protected CoreModule coreModule;
    @Mock
    protected Retrofit retrofit;
    @Mock
    protected OkHttpClient client;
    @Mock
    protected Application application;
    @Mock
    protected Cache cache;
    protected CoreComponent coreComponent;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        coreComponent = DaggerCoreComponent
                .builder()
                .coreModule(coreModule)
                .netModule(netModule)
                .build();
        PowerMockito.mockStatic(CoreInjector.class, Retrofit.class, Cache.class);
        PowerMockito.when(CoreInjector.from(any(Context.class))).thenReturn(coreComponent);

        when(netModule.provideRetrofit(Matchers.any(Gson.class), Matchers.any(OkHttpClient.class))).thenReturn(retrofit);
        when(netModule.provideGson()).thenReturn(new Gson());
        when(netModule.provideHttpCache(application)).thenReturn(cache);
        when(netModule.provideOkhttpClient(cache)).thenReturn(client);
    }

    @Test
    public void emptyTest() {
        //to compile with Runner
    }
}