package com.android.dhara.githubdemo.home.router;

import android.support.v7.app.AppCompatActivity;

import com.android.dhara.githubdemo.GitHubBaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(PowerMockRunner.class)
public class RepoRouterImplTest extends GitHubBaseTest {
    private RepoRouterImpl router;

    @Mock
    AppCompatActivity activity;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        router = new RepoRouterImpl(activity);
    }

    @Test
    public void testOnBackPressed() {
        router.onBackPressed();
        verify(activity).onBackPressed();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(activity);
    }
}
