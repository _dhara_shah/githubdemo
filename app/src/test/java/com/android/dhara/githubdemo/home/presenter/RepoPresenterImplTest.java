package com.android.dhara.githubdemo.home.presenter;

import com.android.dhara.githubdemo.GitHubBaseTest;
import com.android.dhara.githubdemo.home.model.RepoInteractor;
import com.android.dhara.githubdemo.home.model.RepositoryModelAdapter;
import com.android.dhara.githubdemo.home.router.RepoRouter;
import com.android.dhara.githubdemo.home.view.RepoView;
import com.android.dhara.githubdemo.network.ServiceError;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class RepoPresenterImplTest extends GitHubBaseTest {
    private static final String ERROR = "ERROR";
    private static final int POSITION = 0;

    private RepoPresenterImpl presenter;

    @Mock
    RepoInteractor interactor;
    @Mock
    RepoView view;
    @Mock
    RepoRouter router;
    @Mock
    ServiceError serviceError;
    @Mock
    GitHubRepo gitHubRepo;
    @Captor
    ArgumentCaptor<RepositoryModelAdapter> modelAdapter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(gitHubModule.provideRepoInteractor()).thenReturn(interactor);
        presenter = new RepoPresenterImpl(view, router);
    }

    @Test
    public void testHandleOnViewCreated() {
        presenter.handleOnViewCreated();
        verify(interactor).getRepoList();
        verify(interactor).loadMore();
        verify(view).initViews(modelAdapter.capture(), eq(presenter));
        verify(view).showProgress();
        verify(interactor).fetchRepositories(eq(presenter));
    }

    @Test
    public void testHandleOnBackPressed() {
        presenter.onSupportNavigateUp();
        verify(router).onBackPressed();
    }

    @Test
    public void testOnRepoLoadSuccess() {
        presenter.onSuccess();
        verify(view).hideProgress();
        verify(interactor).getRepoList();
        verify(interactor).loadMore();
        verify(view).updateAdapter(modelAdapter.capture());
    }

    @Test
    public void testOnRepoLoadError() {
        when(serviceError.getErrorMessage()).thenReturn(ERROR);
        presenter.onError(serviceError);
        verify(view).hideProgress();
        verify(serviceError).getErrorMessage();
        verify(view).showError(eq(ERROR));
    }

    @Test
    public void testOnItemClickListener() throws Exception {
        when(interactor.getRepo(POSITION)).thenReturn(gitHubRepo);
        presenter.onClick(POSITION);
        verify(interactor).getRepo(POSITION);
        verify(router).openDetail(gitHubRepo);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(view, interactor, router);
    }

}
