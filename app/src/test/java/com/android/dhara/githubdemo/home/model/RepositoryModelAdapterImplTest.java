package com.android.dhara.githubdemo.home.model;

import com.android.dhara.githubdemo.R;
import com.android.dhara.githubdemo.network.entity.GitHubRepo;
import com.android.dhara.githubdemo.network.entity.RepoOwner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Realm.class)
public class RepositoryModelAdapterImplTest {
    private static final String REPO_NAME = "REPO_NAME";
    private static final String REPO_DESC = "REPO_DESC";
    private static final String REPO_OWNER_NAME = "REPO_OWNER_NAME";
    private static final int POSITION = 0;

    private RepositoryModelAdapterImpl modelAdapter;
    private List<GitHubRepo> repoList;

    @Mock
    GitHubRepo repo;
    @Mock
    RepoOwner repoOwner;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Realm.class);

        when(repo.getRepoName()).thenReturn(REPO_NAME);
        when(repo.getDescription()).thenReturn(REPO_DESC);
        when(repo.getFork()).thenReturn(false);
        when(repo.getOwner()).thenReturn(repoOwner);
        when(repoOwner.getOwnerName()).thenReturn(REPO_OWNER_NAME);

        repoList = new ArrayList<>();
        repoList.add(repo);

        modelAdapter = RepositoryModelAdapterImpl.from(repoList, true);
    }

    @Test
    public void testGetCount() {
        assertEquals(modelAdapter.getCount(), 2);
    }

    @Test
    public void testGetRepoName() {
        assertEquals(modelAdapter.getRepoName(POSITION), REPO_NAME);
        verify(repo).getRepoName();
    }

    @Test
    public void testGetDescription() {
        assertEquals(modelAdapter.getRepoDescription(POSITION), REPO_DESC);
        verify(repo).getDescription();
    }

    @Test
    public void testGetRepoOwnerName() {
        assertEquals(modelAdapter.getRepoOwnerName(POSITION), REPO_OWNER_NAME);
        verify(repo).getOwner();
        verify(repoOwner).getOwnerName();
    }

    @Test
    public void testGetBackgroundColor() {
        when(repo.getFork()).thenReturn(true);
        assertEquals(modelAdapter.getBackgroundColor(POSITION), R.color.colorGreen);
        verify(repo).getFork();
    }

    @Test
    public void testGetItemViewType() {
        assertEquals(modelAdapter.getItemViewType(POSITION), RepositoryModelAdapter.VIEW_TYPE_ROW);
    }

    @Test
    public void testLoadMore() {
        assertTrue(modelAdapter.loadMore());
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(repo, repoOwner);
    }
}
